#!/usr/bin/python
# -*- coding: utf-8 -*-

import tiledtmxloader
import sys
import os
import math
import glob
import re
import pygame
from images import *

# Rotation: see http://www.pygame.org/wiki/RotateCenter

def rot_center(image, rect, angle):
	"""rotate an image while keeping its center"""
	rot_image = pygame.transform.rotate(image, angle)
	rot_rect = rot_image.get_rect(center=rect.center)
	return rot_image,rot_rect

class vehicle(tiledtmxloader.helperspygame.SpriteLayer.Sprite):
	def __init__(self, directory, files):
		self.ch_speed = 100
		self.load(directory, files)
		self.frame_num = 0
		self.angle = 0
		image = self.images[self.frame_num]
		self.setimage(image, image.get_rect())
		super(vehicle, self).__init__(self.image, self.rect)
	def load(self, directory, files):
		self.images = get_images(directory, 'car.png' )
		self.last_tick = pygame.time.get_ticks()
	def lookat(self, angle):
		center = self.rect.center
		if (self.frame_num >= len(self.images)):
				self.frame_num=0;
		if angle is not None:
			self.angle = angle
		image,rect = rot_center(self.images[self.frame_num], self.rect, -self.angle)
		rect.center = center
		self.setimage(image, rect)
		self.rect = rect
	def update(self, inc_x, inc_y, action_type):
		self.ch_speed = 100
		if inc_x == 0 and inc_y == 0:
			angle = None
		else:
			angle = math.atan2(inc_x, -inc_y) * 180.0 / math.pi # 0 degrees is north, positive is east
		self.lookat(angle)
		tick = pygame.time.get_ticks()
		if (tick - self.last_tick > self.ch_speed):
			self.last_tick = tick;
			self.frame_num += 1;
			if (self.frame_num >= len(self.images)):
				self.frame_num=0;
	def setimage(self, image, rect):
		self.image = image
		self.rect = rect
		self.width = rect.width
		self.height = rect.height
	def moveto(self, pos_x, pos_y):
		self.rect.center = (pos_x, pos_y)
