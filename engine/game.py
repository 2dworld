#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import os
import math
import glob
import re

import pygame

try:
	import _path
except:
	pass

import tiledtmxloader

import actor
from hero import avatar_hero

#  -----------------------------------------------------------------------------

def draw_text(screen, font, text, x, y, color, align_right=False):
	surface = font.render(text, True, color)
	surface.set_colorkey( (0, 0, 0) )
	screen.blit(surface, (x, y))

def center_text(font, text, x, y, color):
	surface = font.render(text, True, color)
	surface.set_colorkey( (0, 0, 0) )
	screen.blit(surface, (x - surface.get_width() / 2, y - surface.get_height() / 2))

def init(screen, map_filename, hero):
	# parser the map (it is done here to initialize the
	# window the same size as the map if it is small enough)
	world_map = tiledtmxloader.tmxreader.TileMapParser().parse_decode(map_filename)

	print world_map.properties

	pygame.display.set_caption("Map viewer: " + map_filename + " ( " + str(world_map.pixel_width) +" x "+ str(world_map.pixel_height) + " )")

	screen_width, screen_height = screen.get_size()

	# default starting position
	hero_pos_x = float(world_map.pixel_width) / 2.0
	hero_pos_y = float(world_map.pixel_height) / 2.0

	# load the images using pygame
	resources = tiledtmxloader.helperspygame.ResourceLoaderPygame()
	resources.load(world_map)

	# prepare map rendering
	assert world_map.orientation == "orthogonal"

	# renderer
	renderer = tiledtmxloader.helperspygame.RendererPygame()

	# retrieve the layers
	sprite_layers = tiledtmxloader.helperspygame.get_layers_from_map(resources)

	# filter layers
	sprite_layers = [layer for layer in sprite_layers if not layer.is_object_group]

	# detect special layers
	hero_layer = 0
	metadata_layer_name = None
	metadata_layer = None
	layer_num = 0
	for layer in world_map.layers:
		if not layer.is_object_group:
			if layer.properties.has_key('player'):
				hero_layer = layer_num
				if layer.properties.has_key('control'):
					print "Player Layer: " + layer.name
					metadata_layer_name = layer.properties['control']
					try:
						hero_pos_x = float(layer.properties['startx']) * world_map.tilewidth
					except:
						pass
					try:
						hero_pos_y = float(layer.properties['starty']) * world_map.tileheight
					except:
						pass
			layer_num += 1
	if metadata_layer_name is not None:
		for layer in world_map.layers:
			if not layer.is_object_group:
				if layer.name == metadata_layer_name:
					print "Control Layer: " + layer.name
					metadata_layer = layer
	print [hero_pos_x, hero_pos_y]
	hero.moveto(hero_pos_x, hero_pos_y)

	# get position of hero
	hero_pos_x = hero.rect.centerx
	hero_pos_y = hero.rect.bottom

	# cam_offset is for scrolling
	cam_world_pos_x = hero.rect.centerx
	cam_world_pos_y = hero.rect.centery

	# set initial cam position and size
	renderer.set_camera_position_and_size(cam_world_pos_x, cam_world_pos_y, \
						screen_width, screen_height)

	# add the hero the the right layer, it can be changed using 0-9 keys
	sprite_layers[hero_layer].add_sprite(hero)

	# create dictionary with the properties of every tile, indexed by gid
	tile_properties = {}
	for tile_set in world_map.tile_sets:
		for tile in tile_set.tiles:
			tile_properties[int(tile_set.firstgid) + int(tile.id)] = tile.properties
	print tile_properties

	return renderer, sprite_layers, metadata_layer, tile_properties, hero_pos_x, hero_pos_y

def loop(screen, renderer, sprite_layers, metadata_layer, tile_properties, hero, hero_pos_x, hero_pos_y):
	# layer add/remove hero keys
	num_keys = [pygame.K_0, pygame.K_1, pygame.K_2, pygame.K_3, pygame.K_4, \
			pygame.K_5, pygame.K_6, pygame.K_7, pygame.K_8, pygame.K_9]

	# define variables for the main loop
	clock = pygame.time.Clock()
	running = True
	speed = 0.075 * 2.
	# set up timer for fps printing
	pygame.time.set_timer(pygame.USEREVENT, 1000)

	speed_factor = 1.0
	action_type = actor.WALK

	font = pygame.font.SysFont("Courier", 20)
	fps_text=""

	# mainloop
	while running:
		dt = clock.tick()

		# event handling
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				running = False
			elif event.type == pygame.USEREVENT:
				fps_text = "fps: " +  ("%.2f" % clock.get_fps())
			elif event.type == pygame.KEYDOWN:
				if event.key == pygame.K_ESCAPE:
					running = False
				elif event.key in num_keys:
					# find out which layer to manipulate
					idx = num_keys.index(event.key)
					# make sure this layer exists
					if idx < len(world_map.layers):
						if sprite_layers[idx].contains_sprite(hero):
							sprite_layers[idx].remove_sprite(hero)
							print("removed hero sprite from layer", idx)
						else:
							sprite_layers[idx].add_sprite(hero)
							print("added hero sprite to layer", idx)
					else:
						print("no such layer or more than 10 layers: " + str(idx))
				elif event.key in (pygame.K_LSHIFT, pygame.K_RSHIFT):
					speed_factor = 2.0
					action_type = actor.RUN
			elif event.type == pygame.KEYUP:
				if event.key in (pygame.K_LSHIFT, pygame.K_RSHIFT):
					speed_factor = 1.0
					action_type = actor.WALK

		# find directions
		direction_x = pygame.key.get_pressed()[pygame.K_RIGHT] - \
						pygame.key.get_pressed()[pygame.K_LEFT]
		direction_y = pygame.key.get_pressed()[pygame.K_DOWN] - \
						pygame.key.get_pressed()[pygame.K_UP]

		# make sure the hero moves with same speed in all directions (diagonal!)
		dir_len = math.hypot(direction_x, direction_y)
		dir_len = dir_len if dir_len else 1.0

		# update position
		step_x = speed_factor * speed * dt * direction_x / dir_len
		step_y = speed_factor * speed * dt * direction_y / dir_len
		if metadata_layer is not None:
			try:
				step_x, step_y = check_movement(hero_pos_x, hero_pos_y, step_x, step_y, \
					hero.width, hero.height, metadata_layer, tile_properties)
			except:
				pass
		hero_pos_x += step_x
		hero_pos_y += step_y
		hero.rect.midbottom = (hero_pos_x, hero_pos_y)
		hero.update(step_x, step_y, action_type)

		if metadata_layer is not None:
			try:
				if check_collision(hero_pos_x, hero_pos_y, hero.width, hero.height, \
						metadata_layer, tile_properties, 'goal'):
					print "GOAL!!!"
			except:
				pass

		draw(screen, renderer, hero, sprite_layers)
		draw_text(screen, font, fps_text, 0, 0, (255, 255, 255))
		pygame.display.flip()


def draw(screen, renderer, hero, sprite_layers):
		# adjust camera according to the hero's position, follow him
		# (don't make the hero follow the cam, maybe later you want different
		#  objects to be followd by the cam)
		renderer.set_camera_position(hero.rect.centerx, hero.rect.centery)

		# clear screen, might be left out if every pixel is redrawn anyway
		screen.fill((0, 0, 0))

		# render the map
		for sprite_layer in sprite_layers:
			if sprite_layer.is_object_group:
				# we dont draw the object group layers
				# you should filter them out if not needed
				continue
			else:
				renderer.render_layer(screen, sprite_layer)

		#tile_width = metadata_layer.tilewidth
		#tile_height = metadata_layer.tileheight
		#tile_x_left = int(math.floor((hero_pos_x - hero.width / 2.0) / tile_width))
		#tile_x_right = int(math.ceil((hero_pos_x + hero.width / 2.0) / tile_width))
		#tile_y_up = int(math.floor((hero_pos_y - hero.height) / tile_height))
		#tile_y_down = int(math.ceil((hero_pos_y) / tile_height))
		#show_rectangle(screen, renderer, hero_pos_x - 2, hero_pos_y - 2, 5, 5)
		#show_rectangle(screen, renderer, tile_x_left * tile_width - 2, tile_y_up * tile_height - 2, 5, 5)
		#show_rectangle(screen, renderer, tile_x_left * tile_width - 2, tile_y_down * tile_height - 2, 5, 5)
		#show_rectangle(screen, renderer, tile_x_right * tile_width - 2, tile_y_up * tile_height - 2, 5, 5)
		#show_rectangle(screen, renderer, tile_x_right * tile_width - 2, tile_y_down * tile_height - 2, 5, 5)


def start(screen, map_filename, hero):
	renderer, sprite_layers, metadata_layer, tile_properties, hero_pos_x, hero_pos_y = init(screen, map_filename, hero)
	loop(screen, renderer, sprite_layers, metadata_layer, tile_properties, hero, hero_pos_x, hero_pos_y)

def show_rectangle(screen, renderer, x, y, w, h):
	pygame.draw.rect(screen, (255, 255, 255), (x - renderer._render_cam_rect.x, y - renderer._render_cam_rect.y, w, h))

#  -----------------------------------------------------------------------------

def check_collision(hero_pos_x, hero_pos_y, hero_width, hero_height, metadata_layer, tile_properties, block_id='block'):
	tile_width = metadata_layer.tilewidth
	tile_height = metadata_layer.tileheight

	half_hero_width = hero_width // 2
	half_hero_height = hero_height // 2

	tile_x_left = int(math.floor((hero_pos_x - half_hero_width) / tile_width))
	tile_x_right = int(math.ceil((hero_pos_x + half_hero_width) / tile_width))
	tile_y_up = int(math.floor((hero_pos_y - hero_height) / tile_height))
	tile_y_down = int(math.ceil((hero_pos_y) / tile_height))

	for y in range(tile_y_up, tile_y_down):
		for x in range(tile_x_left, tile_x_right):
			gid = metadata_layer.content2D[x][y]
			if gid in tile_properties and block_id in tile_properties[gid] and tile_properties[gid][block_id]:
				return True

	return False

def check_movement(hero_pos_x, hero_pos_y, step_x, step_y, \
					hero_width, hero_height, metadata_layer, tile_properties, block_id='block'):
	"""
	Checks collision of the hero against the world. Its not the best way to
	handle collision detection but for this demo it is good enough.

	:Returns: steps to add to heros current position.
	"""
	# create hero rect
	hero_rect = pygame.Rect(0, 0, hero_width, hero_height)
	hero_rect.midbottom = (hero_pos_x, hero_pos_y)

	# Size of the tiles
	tile_width = metadata_layer.tilewidth
	tile_height = metadata_layer.tileheight

	half_hero_width = hero_width // 2
	half_hero_height = hero_height // 2

	tile_x_left = int(math.floor((hero_pos_x - half_hero_width) / tile_width))
	tile_x_right = int(math.ceil((hero_pos_x + half_hero_width) / tile_width))
	tile_y_up = int(math.floor((hero_pos_y - hero_height) / tile_height))
	tile_y_down = int(math.ceil((hero_pos_y) / tile_height))

	if step_x < 0:
		sx = tile_x_left
		ex = int((hero_pos_x - half_hero_width + step_x) // tile_width)
	elif step_x > 0:
		sx = tile_x_right
		ex = int((hero_pos_x + half_hero_width + step_x) // tile_width)
	else:
		sx = int((hero_pos_x) // tile_width)
		ex = sx

	if step_y < 0:
		sy = tile_y_up
		ey = int((hero_pos_y - hero_height + step_y) // tile_height)
	elif step_y > 0:
		sy = tile_y_down
		ey = int((hero_pos_y + step_y) // tile_height)
	else:
		sy = int((hero_pos_y - half_hero_height) // tile_height)
		ey = sy

	for y in range(tile_y_up, tile_y_down):
		gid = metadata_layer.content2D[ex][y]
		if gid in tile_properties and block_id in tile_properties[gid] and tile_properties[gid][block_id]:
			step_x = 0
			ex = sx

	xl = int(math.floor((hero_pos_x - half_hero_width + step_x) / tile_width))
	xr = int(math.ceil((hero_pos_x + half_hero_width + step_x) / tile_width))

	for x in range(xl, xr):
		gid = metadata_layer.content2D[x][ey]
		if gid in tile_properties and block_id in tile_properties[gid] and tile_properties[gid][block_id]:
			step_y = 0
			ey = sy

	# return the step the hero should do
	return step_x, step_y

#  -----------------------------------------------------------------------------

def special_round(value):
	"""
	For negative numbers it returns the value floored,
	for positive numbers it returns the value ceiled.
	"""
	# same as:  math.copysign(math.ceil(abs(x)), x)
	# OR:
	# ## versus this, which could save many function calls
	# import math
	# ceil_or_floor = { True : math.ceil, False : math.floor, }
	# # usage
	# x = floor_or_ceil[val<0.0](val)

	if value < 0:
		return math.floor(value)
	return math.ceil(value)

