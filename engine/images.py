#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import os
import math
import glob
import re
import pygame


def get_files_natural_order(path):
	file_list = []
	for infile in glob.glob( path ):
		if os.path.isfile(infile):
			file_list.append(infile)
	digits = re.compile(r'(\d+)')
	def tokenize(filename):
		return tuple(int(token) if match else token
			for token, match in
				((fragment, digits.search(fragment))
				for fragment in digits.split(filename)))
	file_list.sort(key=tokenize)
	return file_list

def get_images_from_files(file_list):
	image_list = []
	for filename in file_list:
		try:
			image = pygame.image.load(filename)
			print 'Image: ', filename
			image_list.append(image)
		except pygame.error, message:
			print 'Cannot load image: ', filename
	return image_list

def create_image(width, height, red, green, blue, alpha):
	image = pygame.Surface((width, height), pygame.SRCALPHA)
	image.fill((red, green, blue, alpha))
	return image

def get_images(directory, selection):
	if isinstance(selection, (list, tuple)):
		img = []
		for element in selection:
			img.extend(get_images(directory, element))
		return img
	else:
		return get_images_from_files(get_files_natural_order(os.path.join(directory, selection)))

def get_mirror_images(source_images, flip_horizontal, flip_vertical):
	destination_images = []
	for image in source_images:
		destination_images.append(pygame.transform.flip(image, flip_horizontal, flip_vertical))
	return destination_images
