#!/usr/bin/python
# -*- coding: utf-8 -*-

from lab_motor import *

# Este es el programa principal, invocado desde más abajo
def main():
	while True:
		while look() != BLOCK:
			forward()
		right()

# Este es el punto en el que empieza el programa
if __name__ == '__main__':
	# Cambia el directorio activo al directorio en el que está el programa
	abspath = os.path.abspath(__file__)
	dname = os.path.dirname(abspath)
	os.chdir(dname)
	# Ejecuta el programa principal
	init()
	main()

