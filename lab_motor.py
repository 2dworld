#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import os
import math
import engine
import pygame

from hero import avatar_hero

MAP = "maps/lab.tmx"
HERO_DIR = "avatars/crono"

SCREEN_WIDTH = 640
SCREEN_HEIGHT = 480

# Este es el programa principal, invocado desde más abajo
def init():
	"""
	Main method.
	"""
	args = sys.argv[1:]
	if len(args) < 1:
		path_to_map = os.path.join(os.curdir, MAP)
		print(("usage: python %s your_map.tmx\n\nUsing default map '%s'\n" % \
			(os.path.basename(__file__), path_to_map)))
	else:
		path_to_map = args[0]

	# Inicializa pygame y abre una ventana
	pygame.init()
	pygame.display.set_caption("Map viewer")

	global screen, hero
	screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
	# Crea el avatar del heroe
	hero = create_hero(HERO_DIR)

	# Inicializa el mundo
	global renderer, sprite_layers, metadata_layer, tile_properties, hero_pos_x, hero_pos_y
	renderer, sprite_layers, metadata_layer, tile_properties, hero_pos_x, hero_pos_y = engine.game.init(screen, path_to_map, hero)
	#engine.game.loop(screen, renderer, sprite_layers, metadata_layer, tile_properties, hero, hero_pos_x, hero_pos_y)

	global clock
	clock = pygame.time.Clock()

	global direction
	direction = 1

def turn(change):
	global direction
	direction += change
	direction = direction % 4
	direction_x, direction_y = [(0,-1), (1,0), (0,1), (-1,0)][direction]

	hero.update(direction_x, direction_y, action_type = engine.actor.STAND)

	engine.game.draw(screen, renderer, hero, sprite_layers)
	pygame.display.flip()

	# event handling
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			sys.exit(0)
		elif event.type == pygame.KEYDOWN:
			if event.key == pygame.K_ESCAPE:
				sys.exit(0)

def right():
	turn(1)

def left():
	turn(-1)

BLOCK = 1
GOAL = 2

FORWARD = 0
RIGHT = 1
LEFT = -1

def look(dir=FORWARD):
	global screen, renderer, sprite_layers, metadata_layer, tile_properties, hero, hero_pos_x, hero_pos_y, clock

	global direction
	lookdir = (direction + dir) % 4
	direction_x, direction_y = [(0,-1), (1,0), (0,1), (-1,0)][lookdir]

	# make sure the hero moves with same speed in all directions (diagonal!)
	dir_len = math.hypot(direction_x, direction_y)
	dir_len = dir_len if dir_len else 1.0

	# update position
	step_x = direction_x * 4
	step_y = direction_y * 4

	# event handling
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			sys.exit(0)
		elif event.type == pygame.KEYDOWN:
			if event.key == pygame.K_ESCAPE:
				sys.exit(0)

	if metadata_layer is not None:
		try:
			if engine.game.check_collision(hero_pos_x+step_x, hero_pos_y+step_y, hero.width, hero.height, \
					metadata_layer, tile_properties, 'block'):
				return BLOCK
		except:
			pass

	if metadata_layer is not None:
		try:
			if engine.game.check_collision(hero_pos_x+step_x, hero_pos_y+step_y, hero.width, hero.height, \
					metadata_layer, tile_properties, 'goal'):
				return GOAL
		except:
			pass

def forward():
	global screen, renderer, sprite_layers, metadata_layer, tile_properties, hero, hero_pos_x, hero_pos_y, clock
	dt = clock.tick()
	speed = 0.075 * 2.

	global direction
	direction = direction % 4
	direction_x, direction_y = [(0,-1), (1,0), (0,1), (-1,0)][direction]

	# make sure the hero moves with same speed in all directions (diagonal!)
	dir_len = math.hypot(direction_x, direction_y)
	dir_len = dir_len if dir_len else 1.0

	# update position
	step_x = speed * dt * direction_x / dir_len
	step_y = speed * dt * direction_y / dir_len

	if metadata_layer is not None:
		try:
			step_x, step_y = engine.game.check_movement(hero_pos_x, hero_pos_y, step_x, step_y, \
				hero.width, hero.height, metadata_layer, tile_properties)
		except:
			pass

	hero_pos_x += step_x
	hero_pos_y += step_y
	hero.rect.midbottom = (hero_pos_x, hero_pos_y)

	hero.update(step_x, step_y, action_type = engine.actor.WALK)

	engine.game.draw(screen, renderer, hero, sprite_layers)
	pygame.display.flip()

	end = False

	if metadata_layer is not None:
		try:
			if engine.game.check_collision(hero_pos_x, hero_pos_y, hero.width, hero.height, \
					metadata_layer, tile_properties, 'goal'):
				print "YAY!!! GOAL!!!"
				end = True
		except:
			pass

	if end:
		sys.exit(0)

	# event handling
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			sys.exit(0)
		elif event.type == pygame.KEYDOWN:
			if event.key == pygame.K_ESCAPE:
				sys.exit(0)

def create_hero(directory):
	"""
	Creates the hero sprite.
	"""
	hero = avatar_hero(directory)
	#hero = engine.vehicle.vehicle("avatars/vehicles", "car.png")
	return hero

